/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sun.bob.mcalendarviewtest;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.WindowManager;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import sun.bob.mcalendarviewtest.slice.ExpMainAbilitySlice;
import sun.bob.mcalendarviewtest.slice.MainAbilitySlice;
import sun.bob.mcalendarviewtest.slice.TabAbilitySlice;

/**
 * Create on 2021-7-23
 */
public class MainAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        getWindow().clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
//        getWindow().setStatusBarColor(Color.BLACK.getValue());
        super.setMainRoute(ExpMainAbilitySlice.class.getName());
        addActionRoute("tab", MainAbility.class.getName());
        addActionRoute("exp", ExpMainAbilitySlice.class.getName());
//        super.setMainRoute(TabAbilitySlice.class.getName());
//        super.setMainRoute(ExpMainAbilitySlice.class.getName());

    }




}
