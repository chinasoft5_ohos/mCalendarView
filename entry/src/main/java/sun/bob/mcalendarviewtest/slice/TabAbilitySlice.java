/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sun.bob.mcalendarviewtest.slice;

import com.ryan.ohos.extension.event.interfaces.View;
import com.ryan.ohos.extension.event.interfaces.ViewGroup;
import com.ryan.ohos.extension.widget.viewpager.FractionPagerAdapter;
import com.ryan.ohos.extension.widget.viewpager.ViewPager;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.adapters.CalendarViewExpAdapter;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnExpDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthScrollListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarviewtest.ResourceTable;

import java.util.Calendar;

/**
 * Create on 2021-7-23
 */
public class TabAbilitySlice extends AbilitySlice {


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tab);
        ViewPager componentById = (ViewPager) findComponentById(ResourceTable.Id_container);
        componentById.setOffscreenPageLimit(3);
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(((FractionAbility) getAbility()).getFractionManager());
        componentById.setAdapter(mSectionsPagerAdapter);
    }

    public static class PlaceholderFragment extends Fraction {

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        IntentParams intentParams = new IntentParams();
        private Text mPageTitleTv;
        private Text YearMonthTv;
        private ExpCalendarView expCalendarView;
        private boolean ifExpand = true;

        public PlaceholderFragment() {
            intentParams = new IntentParams();
        }


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         *
         * @param sectionNumber 选中index
         * @return fragment
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            fragment.getIntentParams().setParam(ARG_SECTION_NUMBER, sectionNumber);
            return fragment;
        }

        public IntentParams getIntentParams() {
            return intentParams;
        }

        @Override
        protected void onStart(Intent intent) {
            super.onStart(intent);

        }

        @Override
        public Component onComponentAttached(LayoutScatter inflater, ComponentContainer container,
                                             Intent intent) {
            Component rootView = inflater.parse(ResourceTable.Layout_fragment_tab, null, false);

            mPageTitleTv = (Text) rootView.findComponentById(ResourceTable.Id_tab_activity_page_title);
            mPageTitleTv.setText("Page: " + intentParams.getParam(ARG_SECTION_NUMBER) + "　　在此滑动page，避免冲突");

            MCalendarView mCalendarView = ((MCalendarView) rootView.findComponentById(ResourceTable.Id_calendar));
            mCalendarView.hasTitle(true);

            expCalendarView = ((ExpCalendarView) rootView.findComponentById(ResourceTable.Id_calendar_exp));
            YearMonthTv = (Text) rootView.findComponentById(ResourceTable.Id_main_YYMM_TV);
            YearMonthTv.setText(Calendar.getInstance().get(Calendar.YEAR) + "年" + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "月");
            expCalendarView.setOnDateClickListener(new OnExpDateClickListener()).setOnMonthScrollListener(new OnMonthScrollListener() {
                @Override
                public void onPageScrollStateChanged(int i) {

                }

                @Override
                public void onMonthChange(int year, int month) {
                    YearMonthTv.setText(String.format("%d年%d月", year, month));
                }

                @Override
                public void onMonthScroll(float positionOffset) {
                }
            });

            imageInit(rootView);

//            Calendar calendar = Calendar.getInstance();
//            DateData selectedDate = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
//            expCalendarView.markDate(selectedDate);
            expCalendarView.markDate(2016, 10, (Integer) intentParams.getParam(ARG_SECTION_NUMBER));

            return rootView;
        }

        private void imageInit(Component view) {
            final Image expandIV = (Image) view.findComponentById(ResourceTable.Id_main_expandIV);
            expandIV.setClickedListener(new Component.ClickedListener() {

                @Override
                public void onClick(Component component) {
                    if (ifExpand) {
                        CellConfig.Month2WeekPos = CellConfig.middlePosition;
                        CellConfig.ifMonth = false;
                        expandIV.setPixelMap(ResourceTable.Media_icon_arrow_down);
                        expCalendarView.shrink();
                    } else {
                        CellConfig.Week2MonthPos = CellConfig.middlePosition;
                        CellConfig.ifMonth = true;
                        expandIV.setPixelMap(ResourceTable.Media_icon_arrow_up);
                        expCalendarView.expand();
                    }
                    ifExpand = !ifExpand;
                }
            });
        }
    }

    /**
     * A {@link FractionPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FractionPagerAdapter {

        public SectionsPagerAdapter(FractionManager fm) {
            super(fm);
        }

        @Override
        public Fraction getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

}
