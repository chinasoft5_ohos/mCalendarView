/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sun.bob.mcalendarviewtest.slice;

import com.ryan.ohos.extension.widget.Toast;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.adapters.CalendarViewAdapter;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.utils.CalendarUtil;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarviewtest.MainAbility;
import sun.bob.mcalendarviewtest.ResourceTable;

import java.util.ArrayList;

import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Create on 2021-7-23
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZYtest");

    PopupDialog dialog;
    private Text ind;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_mune).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Component menu = LayoutScatter.getInstance(component.getContext()).parse(ResourceTable.Layout_mune, null, false);
                dialog = show(component, menu, MainAbilitySlice.this);
            }
        });
        initData();
    }


    private void initData() {

        final MCalendarView calendarView = ((MCalendarView) findComponentById(ResourceTable.Id_calendar));
        ind = (Text) findComponentById(ResourceTable.Id_ind);
        //      Set up listeners.
        calendarView.travelTo(new DateData(2016, 11, 1));
        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(Component view, DateData date) {
//                Toast.makeText(MainActivity.this, String.format("%d-%d", date.getMonth(), date.getDay()), Toast.LENGTH_SHORT).show();
                HiLog.debug(TAG, "onDateClick : " + String.format("%d月%d日", date.getMonth(), date.getDay()));
                Toast.show(getContext(),String.format("%d月%d日", date.getMonth(), date.getDay()));
            }
        }).setOnMonthChangeListener(new OnMonthChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }

            @Override
            public void onMonthChange(int year, int month) {
                ind.setText(String.format("%d-%d", year, month));
//                Toast.makeText(MainActivity.this, String.format("%d-%d", year, month), Toast.LENGTH_SHORT).show();
//                calendarView.markDate(year, month, 5);
//                MarkedDates.getInstance().notifyObservers();
            }
        }).setMarkedStyle(MarkStyle.RIGHTSIDEBAR)
                .markDate(2016, 2, 1).markDate(2016, 3, 25)
                .markDate(2016, 2, 4)
                .markDate(new DateData(2016, 3, 1).setMarkStyle(new MarkStyle(MarkStyle.DOT, new RgbColor(76, 17, 94, 255))))
                .hasTitle(false);


    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tabactivity:
                present(new TabAbilitySlice(), new Intent());
                break;
            case ResourceTable.Id_expcalendaract:
                present(new ExpMainAbilitySlice(), new Intent());
                break;
        }

        if (dialog != null && dialog.isShowing()) {
            dialog.destroy();
        }
    }


    public PopupDialog show(Component componentShow, Component dialogCompant, Component.ClickedListener clickedListener) {
        Context context = componentShow.getContext();
        PopupDialog commonDialog = new PopupDialog(context, dialogCompant);
        commonDialog.setCustomComponent(dialogCompant);
        if (dialogCompant instanceof ComponentContainer) {
            for (int i = 0; i < ((ComponentContainer) dialogCompant).getChildCount(); i++) {
                ((ComponentContainer) dialogCompant).getComponentAt(i).setClickedListener(clickedListener);
            }
            dialogCompant.setClickedListener(clickedListener);
        } else {
            dialogCompant.setClickedListener(clickedListener);
        }
        commonDialog.setBackColor(Color.TRANSPARENT);
        commonDialog.setAutoClosable(true);
        commonDialog.setTransparent(true);
        int[] local = componentShow.getLocationOnScreen();
        commonDialog.showOnCertainPosition(LayoutAlignment.LEFT | LayoutAlignment.TOP, local[0], local[1]);
        return commonDialog;
    }


}
