/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sun.bob.mcalendarviewtest.slice;

import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.adapters.CalendarViewExpAdapter;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnExpDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthScrollListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.views.LogUtil;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarviewtest.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.Calendar;

/**
 * Create on 2021-7-23
 */
public class ExpMainAbilitySlice extends AbilitySlice {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZYtest");
    private boolean ifExpand = true;
    private ExpCalendarView expCalendarView;
    private DateData selectedDate;
    private Text text;
    private CalendarViewExpAdapter adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.loge("测试进入页面1");
        super.setUIContent(ResourceTable.Layout_exp_main_activity);
        LogUtil.loge("测试进入页面2");
        text = (Text) findComponentById(ResourceTable.Id_main_YYMM_TV);
        DatePicker datePicker = new DatePicker(this);
        text.setText(datePicker.getYear() + "年" + datePicker.getMonth() + "月");
        Calendar calendar1 = Calendar.getInstance();
        int year = calendar1.getWeekYear();

        expCalendarView = (ExpCalendarView) findComponentById(ResourceTable.Id_calendar_exp);

        //      Set up listeners.
        expCalendarView.setOnDateClickListener(new OnExpDateClickListener()).setOnMonthScrollListener(new OnMonthScrollListener() {
            @Override
            public void onPageScrollStateChanged(int i) {

            }

            @Override
            public void onMonthChange(int year, int month) {
                text.setText(String.format("%d年%d月", year, month));
                HiLog.debug(TAG, "onMonthScroll : " + String.format("%d年%d月", year, month));
            }

            @Override
            public void onMonthScroll(float positionOffset) {
                HiLog.debug(TAG, "onMonthScroll:" + positionOffset);
            }
        });

//        onDateClick(CurrentCalendar.getCurrentDateData());
        expCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(Component view, DateData date) {
                HiLog.debug(TAG, "点击修改日期");
                ExpMainAbilitySlice.this.onDateClick(date);
            }
        });
        LogUtil.loge("测试进入页面3");
        imageInit();


        Button button = (Button) findComponentById(ResourceTable.Id_main_btn);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                TravelToClick(component);
            }
        });

        Calendar calendar = Calendar.getInstance();
        selectedDate = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        expCalendarView.markDate(selectedDate);
        LogUtil.loge("测试进入页面4  完成");
    }

    private void onDateClick(DateData date) {
        expCalendarView.getMarkedDates().removeAdd();
        expCalendarView.markDate(date);
        selectedDate = date;
    }

    private void imageInit() {
        Image image = (Image) findComponentById(ResourceTable.Id_main_expandIV);
        image.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (ifExpand) {
                    CellConfig.Month2WeekPos = CellConfig.middlePosition;
                    CellConfig.ifMonth = false;
                    image.setPixelMap(ResourceTable.Media_icon_arrow_down);
//                    expandIV.setImageResource(R.mipmap.icon_arrow_down);
                    CellConfig.weekAnchorPointDate = selectedDate;
                    expCalendarView.shrink();
                } else {
                    image.setPixelMap(ResourceTable.Media_icon_arrow_up);
                    CellConfig.Week2MonthPos = CellConfig.middlePosition;
                    CellConfig.ifMonth = true;
//                    expandIV.setImageResource(R.mipmap.icon_arrow_up);
                    expCalendarView.expand();
                }
                ifExpand = !ifExpand;
            }
        });
    }

    public void TravelToClick(Component component) {
        expCalendarView.travelTo(new DateData(1980, 11, 14));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
