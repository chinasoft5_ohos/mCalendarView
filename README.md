# mCalendarView


#### 项目介绍
- 项目名称：mCalendarView
- 所属系列：ohos的第三方组件适配移植
- 功能：日历小工具
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.0.0
        
#### 效果演示
![](img/demo.gif)

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle

dependencies {
     implementation('com.gitee.chinasoft_ohos:mcalendarview:1.0.0')
}

```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.要使用 mCalendarView，您需要使用 xml 文件或 Java 代码将其添加到布局中
```示例XML
<sun.bob.mcalendarview.views.ExpCalendarView
    ohos:id="$+id/calendar_exp"
    ohos:width="match_parent"
    ohos:height="match_content"/>
```
2.初始化控件
```java
    ExpCalendarView expCalendarView = (ExpCalendarView) findComponentById(ResourceTable.Id_calendar_exp);
```
3.设置跳转到指定日期travelTo().默认情况下，mCalendarView 将显示当前月份。如果您想跳转到另一个月份，请使用travelTo(DateData date)mCalendarView 中的函数
```java
    calendarView.travelTo(new DateData(1992, 5, 5));
```

4.设置 OnMonthChangeListener，它是一个抽象类，当用户向右或向左滑动更改月份时将被调用。它将提供当前年份和月份的值作为参数。
```java
        public abstract void onMonthChange(int year, int month);
```
要设置 OnMonthChangeListener，请扩展此类并setOnMonthChangeListener(OnMonthChangeListener l)在 mCalendarView 中调用函数。
```java
    calendarView.setOnMonthChangeListener(new OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month) {
                new ToastDialog(getContext()).setText(String.format("%d-%d", year, month)).show();
            }
        });
```
5.设置日期选择监听 setOnDateClickListener， 它也是一个抽象类，将在使用单击日期单元格时调用。它将提供一个DateDataas 参数。
```java
    public abstract void onDateClick(View view,DateData date);
```
要设置 OnDateClickListener，请扩展此类并setOnDateClickListener(OnDateClickListener l)在 mCalendarView 中调用函数。
```java
    calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                new ToastDialog(getContext()).setText(String.format("%d-%d", date.getMonth(), date.getDay())).show();
            }
        });
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0
- 0.0.2-SNAPSHOT
- 0.0.1-SNAPSHOT

#### 版权和许可信息
```
Copyright 2015 Bob Sun

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

See LICENSE file for details.
```