package sun.bob.mcalendarview.vo;

import ohos.agp.colors.RgbColor;
import sun.bob.mcalendarview.MarkStyle;

import java.util.Objects;

/**
 * Created by bob.sun on 15/8/27.
 */
public class DateData {
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private MarkStyle markStyle;

    public DateData(int year, int month, int day) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = 0;
        this.minute = 0;
        this.markStyle = new MarkStyle();
    }

    /**
     * getYear
     *
     * @return int
     */
    public int getYear() {
        return year;
    }

    /**
     * setYear
     *
     * @param year
     * @return DateData
     */
    public DateData setYear(int year) {
        this.year = year;
        return this;
    }

    /**
     * getMonth
     *
     * @return int
     */
    public int getMonth() {
        return month;
    }

    /**
     * getMonthString
     *
     * @return String
     */
    public String getMonthString() {
        return month > 9 ? String.format("%d", month) : String.format("0%d", month);
    }

    /**
     * setMonth
     *
     * @param month
     * @return DateData
     */
    public DateData setMonth(int month) {
        this.month = month;
        return this;
    }

    /**
     * getDay
     *
     * @return int
     */
    public int getDay() {
        return day;
    }

    /**
     * getDayString
     *
     * @return String
     */
    public String getDayString() {
        return day > 9 ? String.format("%d", day) : String.format("0%d", day);
    }

    /**
     * setDay
     *
     * @param day
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * getHour
     *
     * @return int
     */
    public int getHour() {
        return hour;
    }

    /**
     * getHourString
     *
     * @return String
     */
    public String getHourString() {
        return hour > 9 ? String.format("%d", hour) : String.format("0%d", hour);
    }

    /**
     * setHour
     *
     * @param hour
     */
    public void setHour(int hour) {
        this.hour = hour;
    }

    /**
     * getMinute
     *
     * @return int
     */
    public int getMinute() {
        return minute;
    }

    /**
     * getMinuteString
     *
     * @return String
     */
    public String getMinuteString() {
        return minute > 9 ? String.format("%d", minute) : String.format("0%d", minute);
    }

    /**
     * setMinute
     *
     * @param minute
     */
    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof DateData) {
            DateData data = (DateData) o;
            return ((data.year == this.year) && (data.month == this.month) && (data.day == this.day));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }

    /**
     * getMarkStyle
     *
     * @return MarkStyle
     */
    public MarkStyle getMarkStyle() {
        return markStyle;
    }

    /**
     * setMarkStyle
     *
     * @param markStyle
     * @return DateData
     */
    public DateData setMarkStyle(MarkStyle markStyle) {
        this.markStyle = markStyle;
        return this;
    }

    /**
     * setMarkStyle
     *
     * @param style
     * @param color
     * @return DateData
     */
    public DateData setMarkStyle(int style, RgbColor color) {
        this.markStyle = new MarkStyle(style, color);
        return this;
    }

    @Override
    public String toString() {
        return "DateData{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                ", markStyle=" + markStyle +
                '}';
    }
}
