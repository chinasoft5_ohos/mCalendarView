package sun.bob.mcalendarview.vo;

/**
 * Created by bob.sun on 15/8/27.
 */
public class DayData {

    private DateData date;
    private int textColor;
    private int textSize;

    public DayData(DateData date) {
        this.date = date;
    }

    /**
     * getTextColor
     *
     * @return int
     */
    public int getTextColor() {
        return textColor;
    }

    /**
     * setTextColor
     *
     * @param textColor
     */
    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    /**
     * getTextSize
     *
     * @return int
     */
    public int getTextSize() {
        return textSize;
    }

    /**
     * setTextSize
     *
     * @param textSize
     */
    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    /**
     * getText
     *
     * @return String
     */
    public String getText() {
        return "" + date.getDay();
    }

    /**
     * getDate
     *
     * @return DateData
     */
    public DateData getDate() {
        return date;
    }

}
