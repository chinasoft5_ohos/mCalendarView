package sun.bob.mcalendarview.vo;

import java.util.ArrayList;
import java.util.Observable;

import sun.bob.mcalendarview.MarkStyle;

/**
 * Created by bob.sun on 15/8/28.
 */
public class MarkedDates extends Observable {
    private static MarkedDates staticInstance;
    private ArrayList<DateData> data;

    private MarkedDates() {
        super();
        data = new ArrayList<>();
    }

    /**
     * MarkedDates
     *
     * @return MarkedDates
     */
    public static MarkedDates getInstance() {
        if (staticInstance == null)
            staticInstance = new MarkedDates();
        return staticInstance;
    }

    /**
     * check
     *
     * @param date
     * @return MarkStyle
     */
    public MarkStyle check(DateData date) {
        int index = data.indexOf(date);
        if (index == -1) {
            return null;
        }
        return data.get(index).getMarkStyle();
    }

    /**
     * remove
     *
     * @param date
     * @return boolean
     */
    public boolean remove(DateData date) {
        this.setChanged();
        this.notifyObservers();
        return data.remove(date);

    }

    /**
     * add
     *
     * @param dateData
     * @return MarkedDates
     */
    public MarkedDates add(DateData dateData) {
//        data.clear();
        data.add(dateData);
        this.setChanged();
        this.notifyObservers();
        return this;
    }

    /**
     * getAll
     *
     * @return ArrayList<DateData>
     */
    public ArrayList<DateData> getAll() {
        return data;
    }

    /**
     * removeAdd
     *
     * @return MarkedDates
     */
    public MarkedDates removeAdd() {
        data.clear();
        this.setChanged();
        this.notifyObservers();
        return this;
    }
}
