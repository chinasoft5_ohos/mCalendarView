package sun.bob.mcalendarview.fragments;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import sun.bob.mcalendarview.ResourceTable;
import sun.bob.mcalendarview.adapters.CalendarAdapter;
import sun.bob.mcalendarview.views.MonthView;
import sun.bob.mcalendarview.vo.MonthData;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MonthFragment extends DirectionalLayout {
    private MonthData monthData;
    private Context context;
    private int cellView = -1;
    private int markView = -1;
    private boolean hasTitle = true;
    private final DirectionalLayout ret;


    public MonthFragment(Context context) {
        super(context, null);
        this.context  = context;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        Component view = layoutScatter.parse(ResourceTable.Layout_month_view, this, true);
        ret = (DirectionalLayout) view.findComponentById(ResourceTable.Id_layout);

    }
//    public static final RgbColor chooseColor = new RgbColor(105, 75, 125,255);
    public void initData(){
        if ((monthData != null) && (monthData.getDate() != null)) {
            if (hasTitle) {
                Text textView = new Text(context);
                textView.setText(String.format("%d-%d", monthData.getDate().getYear(), monthData.getDate().getMonth()));
                ret.addComponent(textView);
            }
            MonthView monthView = new MonthView(getContext());
            monthView.setLayoutConfig(new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
//            ShapeElement shapeElement = new ShapeElement();
//            shapeElement.setShape(ShapeElement.OVAL);
//            shapeElement.setRgbColor(chooseColor);
//            monthView.setBackground(shapeElement);
            monthView.setItemProvider(new CalendarAdapter(getContext(), 1, monthData.getData()).setCellViews(cellView, markView));
            ret.addComponent(monthView);
//            ret.setBackground(shapeElement);

        }
    }

    public void setData(MonthData monthData, int cellView, int markView) {
        this.monthData = monthData;
        this.cellView = cellView;
        this.markView = markView;
        initData();
    }

    public void setTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
    }


}
