package sun.bob.mcalendarview;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Created by bob.sun on 15/8/28.
 */
public class MarkStyle {
    public static final int BACKGROUND = 1;
    public static final int DOT = 2;
    public static final int LEFTSIDEBAR = 3;
    public static final int RIGHTSIDEBAR = 4;
    public static final int TEXT = 5;
    public static final int DEFAULT = 10;

    public static RgbColor defaultColor = new RgbColor(0, 148, 243, 255);

    public static String text;
    public static int textColor;

    public static void setDefaultColor(RgbColor defaultColor) {
        MarkStyle.defaultColor = defaultColor;
    }

    public static void setCurrent(int current) {
        MarkStyle.current = current;
    }

    public static int current = DEFAULT;

    /**
     * ShapeElement todayBackground
     */
    public static ShapeElement todayBackground = new ShapeElement() {
        private Paint paint;

        {
            paint = new Paint();
            paint.setColor(new Color(Color.rgb(63, 81, 200)));
        }

        @Override
        public void drawToCanvas(Canvas canvas) {
            super.drawToCanvas(canvas);
            canvas.drawCircle(canvas.getLocalClipBounds().getWidth() / 2,
                    canvas.getLocalClipBounds().getHeight() / 2,
                    canvas.getLocalClipBounds().getHeight() / 3,
                    paint);
        }
    };

    /**
     * ShapeElement choose
     */
    public static ShapeElement choose = new ShapeElement() {
        private Paint paint;

        {
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.LTGRAY);
        }

        @Override
        public void drawToCanvas(Canvas canvas) {
            super.drawToCanvas(canvas);
            canvas.drawCircle(canvas.getLocalClipBounds().getWidth() / 2,
                    canvas.getLocalClipBounds().getHeight() / 2,
                    canvas.getLocalClipBounds().getHeight() / 3,
                    paint);
        }
    };

    private int style;
    private RgbColor color;

    public MarkStyle() {
        this.style = MarkStyle.DEFAULT;
        this.color = MarkStyle.defaultColor;
    }

    public MarkStyle(int style, RgbColor color) {
        this.style = style;
        this.color = color;
    }

    /**
     * getStyle
     *
     * @return int
     */
    public int getStyle() {
        return style;
    }

    /**
     * setStyle
     *
     * @param style
     * @return MarkStyle
     */
    public MarkStyle setStyle(int style) {
        this.style = style;
        return this;
    }

    /**
     * getColor
     *
     * @return RgbColor
     */
    public RgbColor getColor() {
        return color;
    }

    /**
     * setColor
     *
     * @param color
     * @return MarkStyle
     */
    public MarkStyle setColor(RgbColor color) {
        this.color = color;
        return this;
    }
}
