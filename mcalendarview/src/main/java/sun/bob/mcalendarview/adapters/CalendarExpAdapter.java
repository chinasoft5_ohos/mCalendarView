package sun.bob.mcalendarview.adapters;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.utils.CurrentCalendar;
import sun.bob.mcalendarview.views.*;
import sun.bob.mcalendarview.vo.DayData;
import sun.bob.mcalendarview.vo.MarkedDates;

/**
 * Created by Bigflower on 2015/12/8.
 */
public class CalendarExpAdapter extends BaseItemProvider implements Observer {
    private ArrayList data;
    private int cellView = -1;
    private int markView = -1;
    private Context mContext;

    /**
     * 构造函数
     * @param context
     * @param resource
     * @param data
     */
    public CalendarExpAdapter(Context context, int resource, ArrayList data) {
        this.data = data;
        mContext = context;
        MarkedDates.getInstance().addObserver(this);
    }

    /**
     * 设置cellView,markView
     * @param cellView
     * @param markView
     * @return CalendarExpAdapter
     */
    public CalendarExpAdapter setCellViews(int cellView, int markView) {
        this.cellView = cellView;
        this.markView = markView;
        notifyDataChanged();
        return this;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        if(data != null && i >= 0 && i < data.size()){
            return data.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component ret = null;
        DayData dayData = (DayData) data.get(i);
        if(dayData != null) {
            MarkStyle style = MarkedDates.getInstance().check(dayData.getDate());
            boolean marked = style != null;
            if (marked) {
                dayData.getDate().setMarkStyle(style);
                if (markView > 0) {
                    BaseMarkView baseMarkView = (BaseMarkView) LayoutScatter.getInstance(mContext)
                            .parse(markView, null, false);
                    baseMarkView.setDisplayText(dayData);
                    ret = baseMarkView;
                } else {
                    ret = new DefaultMarkView(mContext);
                    ((DefaultMarkView) ret).setDisplayText(dayData);
                }
            } else {
                if (cellView > 0) {
                    BaseCellView baseCellView = (BaseCellView) LayoutScatter.getInstance(mContext)
                            .parse(cellView, null, false);
                    baseCellView.setDisplayText(dayData);
                    ret = baseCellView;
                } else {
                    ret = new DefaultCellView(mContext);
                    ((DefaultCellView) ret).setTextColor(dayData.getText(), dayData.getTextColor());
                }
            }
            ((BaseCellView) ret).setDate(dayData.getDate());
        }
        if (OnDateClickListener.instance != null) {
            ((BaseCellView) ret).setOnDateClickListener(OnDateClickListener.instance);
        }

        if (dayData.getDate().equals(CurrentCalendar.getCurrentDateData()) &&
                ret instanceof DefaultCellView) {
            ((DefaultCellView) ret).setDateToday();

        }
        return ret;
    }

    @Override
    public void update(Observable observable, Object data) {
        LogUtil.loge("走没走  update");
        this.notifyDataChanged();
    }
}