package sun.bob.mcalendarview.adapters;

import com.ryan.ohos.extension.widget.viewpager.PagerAdapter;
import ohos.agp.components.*;
import ohos.app.Context;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.views.LogUtil;
import sun.bob.mcalendarview.views.MonthExpFragment;

import java.util.ArrayList;
import java.util.List;

public class CalendarViewExpAdapter extends PagerAdapter {
    private static boolean hasTitle = true;
    private List<Component> mList = new ArrayList<>();
    private Context context;

    /**
     * 构造函数
     *
     * @param context
     */
    public CalendarViewExpAdapter(Context context) {
        this.context = context;
//        getData();
    }

    /**
     * 设置标题
     *
     * @param hasTitle
     */
    public static void setHasTitle(boolean hasTitle) {
        CalendarViewExpAdapter.hasTitle = hasTitle;
    }

    /**
     * 设置dateCellRes
     *
     * @param dateCellRes
     * @return CalendarViewExpAdapter
     */
    public CalendarViewExpAdapter setDateCellId(int dateCellRes) {
        return this;
    }


    /**
     * 设置markCellId
     *
     * @param markCellId
     * @return CalendarViewExpAdapter
     */
    public CalendarViewExpAdapter setMarkCellId(int markCellId) {
        return this;
    }

    /**
     * 设置标题
     *
     * @param hasTitle
     * @return CalendarViewExpAdapter
     */
    public CalendarViewExpAdapter setTitle(boolean hasTitle) {
        setHasTitle(hasTitle);
        return this;
    }

    @Override
    public int getCount() {
        return 1000;
    }


    @Override
    public boolean isViewFromObject(Component component, Object o) {
        return component == o;
    }

    @Override
    public int getItemPosition(Object o) {
        return POSITION_NONE;

    }

    @Override
    public Object instantiateItem(ComponentContainer componentContainer, int i) {
        LogUtil.loge("instantiateItem");
        Component c = new MonthExpFragment(context, i);
        componentContainer.addComponent(c);
        return c;
    }

        @Override
    public void destroyItem(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component)o);
    }
    @Override
    public void setPrimaryItem(ComponentContainer container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        ((ExpCalendarView) container).measureCurrentView(position);
    }

    /**
     * 填充数据
     */
    public void getData() {
        for (int i = 0; i < 10; i++) {
            mList.add(new MonthExpFragment(context, i));
        }
    }

}
