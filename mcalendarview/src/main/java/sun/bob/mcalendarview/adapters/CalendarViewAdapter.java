package sun.bob.mcalendarview.adapters;


import com.ryan.ohos.extension.widget.viewpager.PagerAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.app.Context;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.fragments.MonthFragment;
import sun.bob.mcalendarview.utils.CalendarUtil;
import sun.bob.mcalendarview.views.LogUtil;
import sun.bob.mcalendarview.views.MonthExpFragment;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarview.vo.MonthData;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Created by bob.sun on 15/8/27.
 */
public class CalendarViewAdapter extends PagerAdapter {

    private DateData date;
    private List<Component> mList = new ArrayList<>();
    private int dateCellId;
    private int markCellId;
    private boolean hasTitle = true;

    private Context context;
    private int mCurrentPosition = -1;

    public CalendarViewAdapter(Context context) {
        this.context = context;
//     getData();
    }

    public CalendarViewAdapter setDate(DateData date) {
        this.date = date;
        return this;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CalendarViewAdapter setDateCellId(int dateCellRes) {
        this.dateCellId = dateCellRes;
        return this;
    }


    public CalendarViewAdapter setMarkCellId(int markCellId) {
        this.markCellId = markCellId;
        return this;
    }

    @Override
    public int getCount() {
        LogUtil.loge("ZTest      getCount:"+mList.size());
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(Component component, Object o) {
        return  component == o;
    }


    @Override
    public Object instantiateItem(ComponentContainer componentContainer, int i) {
        LogUtil.loge("ZTest      instantiateItem:"+i);
        componentContainer.addComponent(mList.get(i));
        return mList.get(i);
    }

    @Override
    public void destroyItem(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent(mList.get(i));
    }


    public CalendarViewAdapter setTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
        getData();
        notifyDataSetChanged();
        return this;
    }

    /**
     * 填充数据
     */
    public void getData() {
        for (int i = 0; i < 1000; i++) {
//            Text textView = new Text(context);
//            textView.setTextAlignment(CENTER);
//            textView.setTextSize(13, Text.TextSizeType.FP);
//            textView.setText("草泥马哟~~");
//            mList.add(textView);
            int year = CalendarUtil.position2Year(i);
            int month = CalendarUtil.position2Month(i);
            MonthFragment fragment = new MonthFragment(context);
            fragment.setTitle(hasTitle);
            MonthData monthData = new MonthData(new DateData(year, month, month / 2), hasTitle);
            fragment.setData(monthData, dateCellId, markCellId);
            mList.add(fragment);
        }
    }
}
