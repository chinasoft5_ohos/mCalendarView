package sun.bob.mcalendarview.adapters;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.utils.CurrentCalendar;
import sun.bob.mcalendarview.views.BaseCellView;
import sun.bob.mcalendarview.views.BaseMarkView;
import sun.bob.mcalendarview.views.DefaultCellView;
import sun.bob.mcalendarview.views.DefaultMarkView;
import sun.bob.mcalendarview.vo.DayData;
import sun.bob.mcalendarview.vo.MarkedDates;

/**
 * Created by bob.sun on 15/8/27.
 */
public class CalendarAdapter extends BaseItemProvider implements Observer {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZYtest");
    private ArrayList data;
    private static int cellView = -1;
    private static int markView = -1;
    private Context mContext;
    /**
     * 构造函数
     * @param context
     * @param resource
     * @param data
     */
    public CalendarAdapter(Context context, int resource, ArrayList data) {
        super();
        this.data = data;
        mContext = context;
        MarkedDates.getInstance().addObserver(this);
    }

    /**
     * 设置cellView,markView
     * @param cellView
     * @param markView
     * @return CalendarAdapter
     */
    public CalendarAdapter setCellViews(int cellView, int markView){
        setCellView(cellView);
        setMarkView(markView);
        return this;
    }

    /**
     * 设置数据
     * @param data
     */
    public void setData(ArrayList data) {
        this.data = data;
    }

    /**
     * 设置cellView
     * @param cellView
     */
    public static void setCellView(int cellView) {
        CalendarAdapter.cellView = cellView;
    }

    /**
     * 设置markView
     * @param markView
     */
    public static void setMarkView(int markView) {
        CalendarAdapter.markView = markView;
    }



    @Override
    public int getCount(){
        HiLog.debug(TAG, "getCount:"+data.size());
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        HiLog.debug(TAG, "getComponent:"+data.size());
        Component ret = null;
        DayData dayData = (DayData) data.get(i);
        if(dayData != null) {
            MarkStyle style = MarkedDates.getInstance().check(dayData.getDate());
            HiLog.debug(TAG, "style:"+dayData.getDate());
            boolean marked = style != null;
            if (marked) {
                HiLog.debug(TAG, "111111111111111111");
                dayData.getDate().setMarkStyle(style);
                if (markView > 0) {
                    BaseMarkView baseMarkView = (BaseMarkView) LayoutScatter.getInstance(mContext)
                            .parse(markView, null, false);
                    baseMarkView.setDisplayText(dayData);
                    ret = baseMarkView;
                } else {
                    ret = new DefaultMarkView(mContext);
                    ((DefaultMarkView) ret).setDisplayText(dayData);
                }
            } else {
                HiLog.debug(TAG, "22222222222");
                if (cellView > 0) {
                    HiLog.debug(TAG, "3333333");
                    BaseCellView baseCellView = (BaseCellView) LayoutScatter.getInstance(mContext)
                            .parse(cellView, null, false);
                    baseCellView.setDisplayText(dayData);
                    ret = baseCellView;
                } else {
                    HiLog.debug(TAG, "44444");
                    ret = new DefaultCellView(mContext);
                    ((DefaultCellView) ret).setTextColor(dayData.getText(), dayData.getTextColor());
                }
            }
            ((BaseCellView) ret).setDate(dayData.getDate());
        }
        if (OnDateClickListener.instance != null) {
            ((BaseCellView) ret).setOnDateClickListener(OnDateClickListener.instance);
        }

        if (dayData.getDate().equals(CurrentCalendar.getCurrentDateData()) &&
                ret instanceof DefaultCellView) {
            HiLog.debug(TAG, "55555555");
            ((DefaultCellView) ret).setDateToday();

        }
        return ret;
    }

    @Override
    public void update(Observable observable, Object data) {
        this.notifyDataChanged();
    }
}
