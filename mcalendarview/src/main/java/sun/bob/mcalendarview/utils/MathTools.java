package sun.bob.mcalendarview.utils;

/**
 * Created on 2015/12/8.
 */
public class MathTools {
    /**
     * 计算工具
     * @param value
     * @param mod
     * @return ret
     */
    public static int floorMod(int value, int mod) {
        int ret = value%mod;
        if (ret < 0) ret += mod;
        return ret;
    }
}