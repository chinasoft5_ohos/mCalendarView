package sun.bob.mcalendarview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Created by 明明大美女 on 2015/12/10.
 */
public class MarkStyleExp {

    public static final RgbColor chooseColor = new RgbColor(105, 75, 125,255);
    public static final RgbColor lightGrayColor = new RgbColor(245, 245, 245, 255);

    /**
     * ShapeElement choose
     */
    public static ShapeElement choose = new ShapeElement() {
        private Paint paint;

        {
//            paint = new Paint();
//            paint.setAntiAlias(true);
//            paint.setColor(new Color(chooseColor));

            setShape(ShapeElement.OVAL);
            setRgbColor(chooseColor);
        }

        @Override
        public void drawToCanvas(Canvas canvas) {
            canvas.drawCircle(canvas.getLocalClipBounds().getWidth() / 2,
                    canvas.getLocalClipBounds().getHeight() / 2,
                    canvas.getLocalClipBounds().getHeight() / 4,
                    paint);
        }

    };

    /**
     * ShapeElement today
     */
    public static ShapeElement today = new ShapeElement() {
        private Paint paint;

        {
            setShape(ShapeElement.OVAL);
            setRgbColor(lightGrayColor);
        }

        @Override
        public void drawToCanvas(Canvas canvas) {
            canvas.drawCircle(canvas.getLocalClipBounds().getWidth() / 2,
                    canvas.getLocalClipBounds().getHeight() / 2,
                    canvas.getLocalClipBounds().getHeight() / 3,
                    paint);
        }

    };


}