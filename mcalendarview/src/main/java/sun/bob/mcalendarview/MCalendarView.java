package sun.bob.mcalendarview;


import com.ryan.ohos.extension.EstimateHelper;
import com.ryan.ohos.extension.widget.viewpager.ViewPager;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import sun.bob.mcalendarview.adapters.CalendarViewAdapter;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.utils.CalendarUtil;
import sun.bob.mcalendarview.utils.CurrentCalendar;
import sun.bob.mcalendarview.views.LogUtil;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarview.vo.MarkedDates;

import java.util.ArrayList;

import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MCalendarView extends ViewPager {
    private int dateCellViewResId = -1;
    private Component dateCellView = null;
    private int markedStyle = -1;
    private int markedCellResId = -1;
    private Component markedCellView = null;
    private boolean hasTitle = true;

    private boolean initted = false;

    private DateData currentDate;
    private CalendarViewAdapter adapter;

    private int width, height;
    private int currentIndex;


    public MCalendarView(Context context) {
        super(context);
        init(context);
    }

    public MCalendarView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        if (initted) {
            return;
        }
        initted = true;
        if (currentDate == null) {
            currentDate = CurrentCalendar.getCurrentDateData();
        }
        // TODO: 15/8/28 Will this cause trouble when achieved?
//        if (this.getId() == View.NO_ID){
//            this.setId(R.id.calendarViewPager);
//        }


        CellConfig.setCellWidth(CalendarUtil.getDisplayWidthInPx(context) / 7);
        CellConfig.setCellHeight(CalendarUtil.getDisplayWidthInPx(context) / 7);
//        getMarkedDates().add(CurrentCalendar.getCurrentDateData());

    }

    //// TODO: 15/8/28 May cause trouble when invoked after inited
    public MCalendarView travelTo(DateData dateData) {
        this.currentDate = dateData;
        CalendarUtil.date = dateData;
        this.initted = false;
        init(getContext());
        return this;
    }

    public MCalendarView markDate(int year, int month, int day) {
        MarkedDates.getInstance().add(new DateData(year, month, day));
        return this;
    }

    public MCalendarView unMarkDate(int year, int month, int day) {
        MarkedDates.getInstance().remove(new DateData(year, month, day));
        return this;
    }

    public MCalendarView markDate(DateData date) {
        MarkedDates.getInstance().add(date);
        return this;
    }

    public MCalendarView unMarkDate(DateData date) {
        MarkedDates.getInstance().remove(date);
        return this;
    }

    public MarkedDates getMarkedDates() {
        return MarkedDates.getInstance();
    }

    public MCalendarView setDateCell(int resId) {
        adapter.setDateCellId(resId);
        return this;
    }

    public MCalendarView setMarkedStyle(int style, RgbColor color) {
        MarkStyle.setCurrent(style);
        MarkStyle.setDefaultColor(color);
        return this;
    }

    public MCalendarView setMarkedStyle(int style) {
        MarkStyle.current = style;
        return this;
    }

    public MCalendarView setMarkedCell(int resId) {
        adapter.setMarkCellId(resId);
        return this;
    }

    public MCalendarView setOnMonthChangeListener(OnMonthChangeListener listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    public MCalendarView setOnDateClickListener(OnDateClickListener onDateClickListener) {
        OnDateClickListener.instance = onDateClickListener;
        return this;
    }

    public MCalendarView hasTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
        adapter = new CalendarViewAdapter(getContext()).setDate(currentDate);
        adapter.setTitle(hasTitle);
        this.setAdapter(adapter);
        this.setCurrentItem(adapter.getCount()/2);
        return this;
    }
    @Override
    public boolean onEstimateSize(int measureWidthSpec, int measureHeightSpec) {
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);
        LogUtil.loge("获取到的高度为:"+height);
            measureHeightSpec = EstimateHelper.makeEstimateSpec(height,EstimateSpec.PRECISE);
        return  super.onEstimateSize(measureWidthSpec, measureHeightSpec);
    }

    private int measureWidth(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == EstimateSpec.NOT_EXCEED) {
            float destiney = AttrHelper.getDensity(mContext);
            result = (int) (CellConfig.cellWidth * 7);
        } else if (specMode == EstimateSpec.PRECISE) {
            result = specSize;
        } else {
            result = (int) CellConfig.cellHeight;
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == EstimateSpec.NOT_EXCEED) {
            int columns = CalendarUtil.getWeekCount(currentIndex);
            columns = hasTitle ? columns + 2 : columns;
            float density = AttrHelper.getDensity(mContext);
            result = (int) (CellConfig.cellHeight * columns);
        } else if (specMode == EstimateSpec.PRECISE) {
            result = specSize;
        } else {
            result = (int) CellConfig.cellHeight;
        }
        return result;
    }


    public void measureCurrentView(int currentIndex) {
//        this.currentIndex = currentIndex;
        postLayout();
    }

}
