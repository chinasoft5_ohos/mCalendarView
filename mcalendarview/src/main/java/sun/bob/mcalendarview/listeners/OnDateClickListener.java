package sun.bob.mcalendarview.listeners;

import ohos.agp.components.Component;
import sun.bob.mcalendarview.vo.DateData;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class OnDateClickListener {
    public static OnDateClickListener instance;

    /**
     * 设置OnDateClickListener
     * @param instance
     */
    public static void setInstance(OnDateClickListener instance) {
        OnDateClickListener.instance = instance;
    }

    public abstract void onDateClick(Component component, DateData date);
}
