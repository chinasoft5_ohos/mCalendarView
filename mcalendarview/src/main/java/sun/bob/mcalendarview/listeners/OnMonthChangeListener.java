package sun.bob.mcalendarview.listeners;

import com.ryan.ohos.extension.widget.viewpager.ViewPager;
import ohos.agp.components.PageSlider;
import sun.bob.mcalendarview.utils.CalendarUtil;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class OnMonthChangeListener implements ViewPager.OnPageChangeListener {

    @Override
    public void onPageSelected(int position){
        onMonthChange(CalendarUtil.position2Year(position), CalendarUtil.position2Month(position));
    };



    public abstract void onMonthChange(int year, int month);
}
