package sun.bob.mcalendarview.views;

import com.ryan.ohos.extension.nested.component.NestedListContainer;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.adapters.CalendarAdapter;
import sun.bob.mcalendarview.vo.MonthData;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MonthView extends NestedListContainer {
    private static CalendarAdapter adapter;
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZYtest");

    /**
     * 构造函数
     * @param context
     */
    public MonthView(Context context) {
        super(context);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(7);
        this.setLayoutManager(tableLayoutManager);
        setLongClickable(false);
        HiLog.debug(TAG,"listView create");
    }

    /**
     * 构造函数
     * @param context
     * @param attrs
     */
    public MonthView(Context context, AttrSet attrs) {
        super(context, attrs);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(7);
        this.setLayoutManager(tableLayoutManager);
        setLongClickable(false);
        HiLog.debug(TAG,"listView create");
    }



    /**
     * 设置数据
     * @param monthData
     * @return MonthView
     */
    public MonthView displayMonth(MonthData monthData){
//        adapter = new CalendarAdapter(getContext(), 1, monthData.getData());
        return this;
    }

}
