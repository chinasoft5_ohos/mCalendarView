package sun.bob.mcalendarview.views;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyleExp;
import sun.bob.mcalendarview.vo.DayData;

import java.util.logging.Logger;

import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Created by bob.sun on 15/8/28.
 */
public class DefaultCellView extends BaseCellView{
    public Text textView;

    /**
     * 构造函数
     * @param context
     */
    public DefaultCellView(Context context) {
        super(context);
        initLayout();
    }

    /**
     * 构造函数
     * @param context
     * @param attrs
     */
    public DefaultCellView(Context context, AttrSet attrs) {
        super(context, attrs);
        initLayout();
    }

    /**
     * 初始化布局
     */
    private void initLayout(){
        this.setLayoutConfig(new DirectionalLayout.LayoutConfig((int) CellConfig.cellWidth, (int) CellConfig.cellHeight));
        this.setOrientation(VERTICAL);
        textView = new Text(getContext());
        textView.setTextSize(13, Text.TextSizeType.FP);
        textView.setLayoutConfig(new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, 0, CENTER, (float) 1.0));
        textView.setTextAlignment(CENTER);
        this.addComponent(textView);
    }

    @Override
    public void setDisplayText(DayData day) {
        textView.setText(day.getText());
    }

    /**
     * 设置被选择的日期
     * @return boolean
     */
    public boolean setDateChoose() {
        LogUtil.loge("ccccccacacacacacacaacacac");
        this.setPadding(20, 20, 20, 20);
        textView.setBackground(MarkStyleExp.choose);
        textView.setTextColor(Color.WHITE);
        return true ;
    }

    /**
     * 设置当天被选择
     */
    public void setDateToday(){
        setPadding(20, 20, 20, 20);
        textView.setBackground(MarkStyleExp.today);
    }

    /**
     * 设置日期默认显示
     */
    public void setDateNormal() {
        textView.setTextColor(Color.BLACK);

        textView.setBackground(null);
    }

    public void setTextColor(String text, int color) {
        textView.setText(text);
        if (color != 0) {
            textView.setTextColor(new Color(color));
        }
    }
}
