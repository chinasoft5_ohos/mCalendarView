package sun.bob.mcalendarview.views;

import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.adapters.CalendarExpAdapter;
import sun.bob.mcalendarview.vo.MonthWeekData;

/**
 * Created by Bigflower on 2015/12/8.
 */
public class MonthViewExpd extends MonthView {
    private MonthWeekData monthWeekData;
    private CalendarExpAdapter adapter;

    /**
     * 构造函数
     * @param context
     */
    public MonthViewExpd(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     * @param context
     * @param attrs
     */
    public MonthViewExpd(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * 初始化adapter
     * @param pagePosition
     * @param cellView
     * @param markView
     */
    public void initMonthAdapter(int pagePosition, int cellView, int markView) {
        getMonthWeekData(pagePosition);
        adapter = new CalendarExpAdapter(getContext(), 1, monthWeekData.getData()).setCellViews(cellView, markView);
        this.setItemProvider(adapter);
    }

    /**
     * 获取数据
     * @param position
     */
    private void getMonthWeekData(int position) {
        monthWeekData = new MonthWeekData(position);
    }
}
