package sun.bob.mcalendarview.views;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class BaseMarkView extends BaseCellView{

    public BaseMarkView(Context context) {
        super(context);
    }

    public BaseMarkView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

}
