package sun.bob.mcalendarview.views;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import sun.bob.mcalendarview.ResourceTable;
import sun.bob.mcalendarview.utils.CalendarUtil;


/**
 * Created by 明明大美女 on 2015/12/8.
 */
public class MonthExpFragment extends DirectionalLayout {
    private int cellView = -1;
    private int markView = -1;
    private int pagePosition ;
    private MonthViewExpd monthViewExpd;
    private static boolean ifExpand = false;

    /**
     * 构造函数
     * @param context
     * @param position
     */
    public MonthExpFragment(Context context, int position) {
        super(context, null);
        pagePosition = position;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        Component view = layoutScatter.parse(ResourceTable.Layout_layout_mark_cell, this, true);
        DirectionalLayout ret = (DirectionalLayout) view.findComponentById(ResourceTable.Id_layout);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        ret.setBackground(shapeElement);
        DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(
                CalendarUtil.getDisplayWidthInPx(context), CalendarUtil.getDisplayWidthInPx(context) * 6 / 7);
        ret.setLayoutConfig(config);
        monthViewExpd = new MonthViewExpd(context);
        monthViewExpd.initMonthAdapter(pagePosition, cellView, markView);
        ret.addComponent(monthViewExpd);
    }

    /**
     * 设置数据
     * @param pagePosition
     * @param cellView
     * @param markView
     */
    public void setData(int pagePosition, int cellView, int markView) {
        this.pagePosition = pagePosition;
        this.cellView = cellView;
        this.markView = markView;
    }

}
