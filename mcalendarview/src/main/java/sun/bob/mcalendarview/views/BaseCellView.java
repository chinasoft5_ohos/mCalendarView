package sun.bob.mcalendarview.views;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarview.vo.DayData;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class BaseCellView extends DirectionalLayout implements Component.EstimateSizeListener {
    private OnDateClickListener clickListener;
    private DateData date;
    int width, height;
    float density;

    /**
     * 构造函数
     * @param context
     */
    public BaseCellView(Context context) {
        super(context);
        density = AttrHelper.getDensity(context);
    }

    /**
     * 构造函数
     * @param context
     * @param attrs
     */
    public BaseCellView(Context context, AttrSet attrs) {
        super(context, attrs);
        density = AttrHelper.getDensity(context);
    }

    /**
     * 设置数据
     * @param date
     * @return BaseCellView
     */
    public BaseCellView setDate(DateData date){
        this.date = date;
        return this;
    }

    /**
     * 设置日期点击监听
     * @param clickListener
     * @return BaseCellView
     */
    public BaseCellView setOnDateClickListener(OnDateClickListener clickListener){
        this.clickListener = clickListener;
        this.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (BaseCellView.this.clickListener != null) {
                    BaseCellView.this.clickListener.onDateClick(BaseCellView.this, date);
                }
            }
        });
        return this;
    }

    /**
     * 移除监听
     * @return BaseCellView
     */
    public BaseCellView removeOnDateClickListener(){
        this.clickListener = null;
        return this;
    }

    /**
     * 获取监听
     * @return OnDateClickListener
     */
    public OnDateClickListener getOnDateClickListener(){
        return this.clickListener;
    }

    @Override
    public boolean onEstimateSize(int measureWidthSpec,int measureHeightSpec){
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);

        measureHeightSpec = EstimateSpec.getChildSizeWithMode(height, height, EstimateSpec.UNCONSTRAINT);
        measureWidthSpec = EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.UNCONSTRAINT);
        setEstimatedSize(measureWidthSpec, measureHeightSpec);
        return true;
    }

    /**
     * 测量宽
     * @param measureSpec
     * @return 宽
     */
    private int measureWidth(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);

        int result = 0;
        if (specMode == EstimateSpec.NOT_EXCEED) {
            result = (int) (CellConfig.cellWidth * density);
        } else if (specMode == EstimateSpec.PRECISE) {
            result = (int) (specSize * density);
        } else {
            result = (int) CellConfig.cellHeight;
        }
        return result;
    }

    /**
     * 测量高
     * @param measureSpec
     * @return 高
     */
    private int measureHeight(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);

        int result = 0;
        if (specMode == EstimateSpec.NOT_EXCEED) {
            result = (int) (CellConfig.cellHeight * density);
        } else if (specMode == EstimateSpec.PRECISE) {
            result = (int) (specSize * density);
        } else {
            result = (int) CellConfig.cellHeight;
        }
        return result;
    }
    public abstract void setDisplayText(DayData day);
}
