package sun.bob.mcalendarview.views;

import com.ryan.ohos.extension.EstimateHelper;
import com.ryan.ohos.extension.widget.viewpager.ViewPager;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.app.Context;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.adapters.CalendarViewExpAdapter;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.listeners.OnMonthScrollListener;
import sun.bob.mcalendarview.utils.CalendarUtil;
import sun.bob.mcalendarview.utils.CurrentCalendar;
import sun.bob.mcalendarview.vo.DateData;
import sun.bob.mcalendarview.vo.MarkedDates;

import java.util.Calendar;


/**
 * Created by 明明大美女 on 2015/12/8.
 */
public class ExpCalendarView extends ViewPager{
    private static int dateCellViewResId = -1;
    private static int markedStyle = -1;
    private static int markedCellResId = -1;
    private static boolean hasTitle = true;
    private boolean initted = false;
    private DateData currentDate;
    private CalendarViewExpAdapter adapter;
    private int width;
    private int height;


    /**
     * 构造函数
     *
     * @param context
     */
    public ExpCalendarView(Context context) {
        super(context);
        init(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrs
     */
    public ExpCalendarView(Context context, AttrSet attrs) {
        super(context, attrs);
        LogUtil.loge("测试日历每周1");
        init(context);
        LogUtil.loge("测试日历每周2");
    }

    /**
     * 设置标题
     *
     * @param hasTitle
     */
    public static void setHasTitle(boolean hasTitle) {
        ExpCalendarView.hasTitle = hasTitle;
    }

    /**
     * 初始化
     *
     * @param context
     */
    public void init(Context context) {
        if (initted) {
            return;
        }
        initted = true;
        if (currentDate == null) {
            currentDate = CurrentCalendar.getCurrentDateData();
        }

        CellConfig.setCellWidth(CalendarUtil.getDisplayWidthInPx(context) / 7);
        CellConfig.setCellHeight(CalendarUtil.getDisplayWidthInPx(context) / 7);
        initDate();
    }

    public void initDate() {
        adapter = new CalendarViewExpAdapter(getContext());
        this.setAdapter(adapter);
        this.setCurrentItem(500);
    }

    /**
     * 滑到指定时间
     *
     * @param dateData
     * @return ExpCalendarView
     * @throws RuntimeException
     */
    //// TODO: 15/8/28 May cause trouble when invoked after inited
    public ExpCalendarView travelTo(DateData dateData) {
        // 获得当前页面的年月（position=500）
        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        int thisMonth = calendar.get(Calendar.MONTH);
        int realPosition = 500 + (dateData.getYear() - thisYear) * 12 + (dateData.getMonth() - thisMonth - 1);
        if (realPosition > 1000 || realPosition < 0) {
            throw new RuntimeException("Please travelto a right date: today-500~today~today+500");
        }

        // 来个步进滑动？因为一次滑个几百页，界面有时候不刷新（蛋疼）
        for (int i = getCurrentItem(); i < realPosition; i = i + 50) {
            setCurrentItem(i);
        }
        for (int i = getCurrentItem(); i > realPosition; i = i - 50) {
            setCurrentItem(i);
        }
        setCurrentItem(realPosition);
        MarkedDates.getInstance().removeAdd();
        // 标记
        MarkedDates.getInstance().add(dateData);
        return this;
    }

    /**
     * 日历展开
     */
    public void expand() {
//        setHeight(measureHeight());
        if (adapter != null) {
            LogUtil.loge("展开展开");
            adapter.notifyDataSetChanged();
        }
//        MarkedDates.getInstance().notifyObservers();
//        postLayout();
//        initDate();
    }

    /**
     * 日历收缩
     */
    public void shrink() {
//        setHeight(measureHeight());
        if (adapter != null) {
            LogUtil.loge("收齐收起");
            adapter.notifyDataSetChanged();
        }
//        MarkedDates.getInstance().notifyObservers();
//        postLayout();
//        initDate();
    }

    /**
     * 标记时间
     *
     * @param year
     * @param month
     * @param day
     * @return ExpCalendarView
     */
    public ExpCalendarView markDate(int year, int month, int day) {
        MarkedDates.getInstance().add(new DateData(year, month, day));
        return this;
    }

    /**
     * 解除标记
     *
     * @param year
     * @param month
     * @param day
     * @return ExpCalendarView
     */
    public ExpCalendarView unMarkDate(int year, int month, int day) {
        MarkedDates.getInstance().remove(new DateData(year, month, day));
        return this;
    }

    /**
     * 标记时间
     *
     * @param date
     * @return ExpCalendarView
     */
    public ExpCalendarView markDate(DateData date) {
        MarkedDates.getInstance().add(date);
        return this;
    }

    /**
     * 解除标记
     *
     * @param date
     * @return ExpCalendarView
     */
    public ExpCalendarView unMarkDate(DateData date) {
        MarkedDates.getInstance().remove(date);
        return this;
    }

    /**
     * 获取标记时间
     *
     * @return ExpCalendarView
     */
    public MarkedDates getMarkedDates() {
        return MarkedDates.getInstance();
    }

    /**
     * 设置dateCell
     *
     * @param resId
     * @return ExpCalendarView
     */
    public ExpCalendarView setDateCell(int resId) {
        adapter.setDateCellId(resId);
        return this;
    }


    /**
     * 设置标记样式
     *
     * @param style
     * @param color
     * @return ExpCalendarView
     */
    public ExpCalendarView setMarkedStyle(int style, RgbColor color) {
        MarkStyle.setCurrent(style);
        MarkStyle.setDefaultColor(color);
        return this;
    }

    /**
     * 设置标记样式
     *
     * @param style
     * @return ExpCalendarView
     */
    public ExpCalendarView setMarkedStyle(int style) {
        MarkStyle.setCurrent(style);
        return this;
    }

    /**
     * 设置markedCell
     *
     * @param resId
     * @return ExpCalendarView
     */
    public ExpCalendarView setMarkedCell(int resId) {
        adapter.setMarkCellId(resId);
        return this;
    }

    /**
     * 设置月份变化监听
     *
     * @param listener
     * @return ExpCalendarView
     */
    public ExpCalendarView setOnMonthChangeListener(OnMonthChangeListener listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    /**
     * 设置月份滑动监听
     *
     * @param listener
     * @return ExpCalendarView
     */
    public ExpCalendarView setOnMonthScrollListener(OnMonthScrollListener listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    /**
     * 设置日期选择监听
     *
     * @param onDateClickListener
     * @return ExpCalendarView
     */
    public ExpCalendarView setOnDateClickListener(OnDateClickListener onDateClickListener) {
        OnDateClickListener.setInstance(onDateClickListener);
        return this;
    }

    /**
     * 设置标题
     *
     * @param hasTitle
     * @return ExpCalendarView
     */
    public ExpCalendarView hasTitle(boolean hasTitle) {
        setHasTitle(hasTitle);
        adapter.setTitle(hasTitle);
        return this;
    }



    @Override
    public boolean onEstimateSize(int measureWidthSpec, int measureHeightSpec) {
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);
        LogUtil.loge("获取到的高度为:"+height);
        measureHeightSpec = EstimateHelper.makeEstimateSpec(height,EstimateSpec.PRECISE);
        return  super.onEstimateSize(measureWidthSpec, measureHeightSpec);
    }

    private int measureWidth(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == EstimateSpec.NOT_EXCEED) {
            LogUtil.loge("获取到的宽度为:1");
            result = (int) (CellConfig.cellWidth * 7);
        } else if (specMode == EstimateSpec.PRECISE) {
            LogUtil.loge("获取到的宽度为:2");
            result = specSize;
        } else {
            LogUtil.loge("获取到的宽度为:3");
            result = (int) CellConfig.cellHeight;
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = EstimateSpec.getMode(measureSpec);
        int specSize = EstimateSpec.getSize(measureSpec);
        if (specMode == EstimateSpec.NOT_EXCEED) {
            if (CellConfig.ifMonth)
                return (int) (CellConfig.cellHeight * 6 );
            else
                return (int) (CellConfig.cellHeight);
        } else if (specMode == EstimateSpec.PRECISE) {
            return specSize;
        } else {
            return (int) CellConfig.cellHeight;
        }
    }

    /**
     * 测量当前视图
     *
     * @param currentIndex
     */
    public void measureCurrentView(int currentIndex) {
        LogUtil.loge("我走了测量!!");
        postLayout();
    }

}

