package sun.bob.mcalendarview.views;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.vo.DayData;

import java.util.logging.Logger;

import static ohos.agp.components.DependentLayout.LayoutConfig.CENTER_IN_PARENT;
import static ohos.agp.components.DependentLayout.LayoutConfig.TRUE;
import static ohos.agp.utils.TextAlignment.CENTER;

/**
 * Created by bob.sun on 15/8/28.
 */
public class DefaultMarkView extends BaseMarkView {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZYtest");
    private Text textView;
    private LayoutConfig matchParentParams;
    private ShapeElement circleDrawable;
    private Context mContext;

    /**
     * 构造函数
     * @param context
     */
    public DefaultMarkView(Context context) {
        super(context);
        mContext = context;
    }

    /**
     * 构造函数
     * @param context
     * @param attrs
     */
    public DefaultMarkView(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    /**
     * 初始化布局和style
     * @param style
     * @throws IllegalArgumentException
     */
    private void initLayoutWithStyle(MarkStyle style){
        HiLog.debug(TAG, "initLayoutWithStyle");
        textView = new Text(mContext);
        textView.setTextAlignment(CENTER);
        textView.setTextSize(13, Text.TextSizeType.FP);
        matchParentParams = new LayoutConfig((int) CellConfig.cellWidth, (int) CellConfig.cellHeight);
        switch (style.getStyle()){
            case MarkStyle.DEFAULT:
            case MarkStyle.BACKGROUND:
                HiLog.debug(TAG, "bbbbbbbbbbbbb");
                this.setLayoutConfig(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setTextColor(Color.WHITE);
                circleDrawable = new ShapeElement();
                circleDrawable.setShape(ShapeElement.OVAL);
                circleDrawable.setRgbColor(style.getColor());
                this.setPadding(20, 20, 20, 20);
                textView.setLayoutConfig(new LayoutConfig(0,
                        ComponentContainer.LayoutConfig.MATCH_PARENT, CENTER,(float) 1.0));
                textView.setBackground(circleDrawable);
                this.addComponent(textView);
                return;
            case MarkStyle.DOT:
                HiLog.debug(TAG, "dddddddddddddd");
                this.setLayoutConfig(matchParentParams);
                this.setOrientation(VERTICAL);
                textView.setLayoutConfig(new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        -2,CENTER, (float) 2.0));
                this.addComponent(new PlaceHolderVertical(getContext()));
                this.addComponent(textView);
                this.addComponent(new Dot(getContext(), style.getColor()));
                return;
            case MarkStyle.RIGHTSIDEBAR:
                HiLog.debug(TAG, "aaaaaaaaaaaaa");
                this.setLayoutConfig(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setLayoutConfig(new LayoutConfig(0,
                        ComponentContainer.LayoutConfig.MATCH_PARENT, CENTER,(float) 3.0));

                this.addComponent(new PlaceHolderHorizontal(getContext()));
                this.addComponent(textView);
                PlaceHolderHorizontal barRight = new PlaceHolderHorizontal(getContext());
                ShapeElement seRight = new ShapeElement();
                seRight.setRgbColor(new RgbColor(style.getColor()));
                barRight.setBackground(seRight);
                this.addComponent(barRight);
                return;
            case MarkStyle.LEFTSIDEBAR:
                HiLog.debug(TAG, "cccccccccccccccccccc");
                this.setLayoutConfig(matchParentParams);
                this.setOrientation(HORIZONTAL);
                textView.setLayoutConfig(new LayoutConfig(0,
                        ComponentContainer.LayoutConfig.MATCH_PARENT, CENTER, (float) 3.0));

                PlaceHolderHorizontal barLeft = new PlaceHolderHorizontal(getContext());
                ShapeElement seLeft = new ShapeElement();
                seLeft.setRgbColor(new RgbColor(style.getColor()));
                barLeft.setBackground(seLeft);
                this.addComponent(barLeft);
                this.addComponent(textView);
                this.addComponent(new PlaceHolderHorizontal(getContext()));

                return;
            default:
                throw new IllegalArgumentException("Invalid Mark Style Configuration!");
        }
    }

    @Override
    public void setDisplayText(DayData day) {
        initLayoutWithStyle(day.getDate().getMarkStyle());
        textView.setText(day.getText());
    }

    /**
     * 水平布局
     */
    static class PlaceHolderHorizontal extends Component{
        LayoutConfig params;

        /**
         * 构造函数
         * @param context
         */
        public PlaceHolderHorizontal(Context context) {
            super(context);
            params = new LayoutConfig(0, ComponentContainer.LayoutConfig.MATCH_PARENT,CENTER, (float) 1.0);
            this.setLayoutConfig(params);
        }

        /**
         * 构造函数
         * @param context
         * @param attrs
         */
        public PlaceHolderHorizontal(Context context, AttrSet attrs) {
            super(context, attrs);
            params = new LayoutConfig(0, ComponentContainer.LayoutConfig.MATCH_PARENT,CENTER, (float) 1.0);
            this.setLayoutConfig(params);
        }
    }

    /**
     * 垂直布局
     */
    static class PlaceHolderVertical extends Component{
        LayoutConfig params;

        /**
         * 构造函数
         * @param context
         */
        public PlaceHolderVertical(Context context) {
            super(context);
            params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,0,CENTER,  (float) 1.0);
            this.setLayoutConfig(params);
        }

        /**
         * 构造函数
         * @param context
         * @param attrs
         */
        public PlaceHolderVertical(Context context, AttrSet attrs) {
            super(context, attrs);
            params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, 0,CENTER , (float) 1.0);
            this.setLayoutConfig(params);
        }
    }

    /**
     * 日期被选择的显示样式
     */
    static class Dot extends DependentLayout{
        /**
         * 构造函数
         * @param context
         * @param color
         */
        public Dot(Context context, RgbColor color) {
            super(context);
            this.setLayoutConfig(new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                    0,0,(float) 1.0));

            Component dotView = new Component(getContext());
            LayoutConfig lp = new LayoutConfig(10, 10);
            lp.addRule(CENTER_IN_PARENT,TRUE);
            dotView.setLayoutConfig(lp);
            ShapeElement dot = new ShapeElement();
            dot.setShape(ShapeElement.OVAL);
            dot.setRgbColor(color);
            dotView.setBackground(dot);
            this.addComponent(dotView);
        }
    }
}
