package sun.bob.mcalendarview.views;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import sun.bob.mcalendarview.utils.ExpCalendarUtil;

import static ohos.agp.utils.TextAlignment.CENTER;


/**
 * Created by Bigflower on 2015/12/8.
 */
public class WeekColumnView extends DirectionalLayout {

    private int backgroundColor = Color.rgb(105, 75, 125);
    private int startendTextColor = Color.rgb(188, 150, 211);
    private int midTextColor = Color.WHITE.getValue();
    private Context mContext;
    private Text[] textView = new Text[7];

    /**
     * 构造函数
     *
     * @param context
     */
    public WeekColumnView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrs
     */
    public WeekColumnView(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context;
        LogUtil.loge("测试初始化日历控件1");
        initAttributeSet(context, attrs);
        LogUtil.loge("测试初始化日历控件2");
        init();
        LogUtil.loge("测试初始化日历控件3");
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrs
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            return;
        }
    }

    /**
     * 初始化
     */
    private void init() {
        initParams();
        initLayout();
        initViews();
    }

    /**
     * 初始化颜色
     */
    private void initParams() {
        backgroundColor = Color.WHITE.getValue();
        startendTextColor = Color.LTGRAY.getValue();
        midTextColor = Color.LTGRAY.getValue();
    }

    /**
     * 初始化布局
     */
    private void initLayout() {
        this.setOrientation(HORIZONTAL);
        ShapeElement se = new ShapeElement();
        se.setRgbColor(new RgbColor(backgroundColor));
        this.setBackground(se);
    }

    /**
     * 初始化视图
     */
    private void initViews() {
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(0, ComponentContainer.LayoutConfig.MATCH_PARENT, LayoutAlignment.CENTER, 1);

        textView[0] = new Text(mContext);
        textView[0].setText(ExpCalendarUtil.number2Week(7));
        textView[0].setTextColor(new Color(startendTextColor));
        textView[0].setLayoutConfig(layoutConfig);
        textView[0].setTextAlignment(CENTER);
        textView[0].setTextSize(36);
        this.addComponent(textView[0]);

        for (int i = 1; i < 6; i++) {
            textView[i] = new Text(mContext);
            textView[i].setTextAlignment(CENTER);
            textConfig(textView[i], ExpCalendarUtil.number2Week(i), midTextColor);
            textView[i].setLayoutConfig(layoutConfig);
            textView[i].setTextSize(36);
            this.addComponent(textView[i]);
        }

        textView[6] = new Text(getContext());
        textView[6].setText(ExpCalendarUtil.number2Week(6));
        textView[6].setTextColor(new Color(startendTextColor));
        textView[6].setTextAlignment(CENTER);
        textView[6].setTextAlignment(CENTER);
        textView[6].setTextSize(36);
        textView[6].setLayoutConfig(layoutConfig);
        this.addComponent(textView[6]);
    }

    /**
     * is this nessesary ?
     *
     * @param textview
     * @param text
     * @param textColor
     */
    private void textConfig(Text textview, String text, int textColor) {
        textview.setText(text);
        textview.setTextColor(new Color(textColor));
    }

    // TODO  public text, textColor, height, backgroundColor

}
